package com.dalaoyang.controller;

import com.dalaoyang.api.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class TestController {

    @Reference
    private UserService userService;

    //灰度用户 http://localhost:8881/testUser?userId=3333&version=2.0.0
    //正常用户 http://localhost:8881/testUser?userId=10&version=2.0.0
    @GetMapping("/testUser")
    public String testUser(Long userId, String version) {
        RpcContext.getContext().setAttachment("userId", Objects.nonNull(userId) ? userId.toString() : "");
        return userService.testUser(userId, version);
    }
}
